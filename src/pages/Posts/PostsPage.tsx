import React, { useEffect, useState } from 'react';
import { getPosts } from '../../api/posts';
import { PostModel } from '../../../server/models/PostModel';
import map from 'lodash/map';
import {useHistory, useParams} from 'react-router-dom';
import { PostItem } from '../../components/PostItem/PostItem';

import './PostsPage.css';

export function usePosts(slug:string='',authorId:string='') {

  const [posts, setPosts] = useState([] as Array<Partial<PostModel>>);

  useEffect(() => {
    getPosts({slug,authorId}).then(setPosts);
  }, [slug,authorId]);

  return posts;
}

export const PostsPage = () => {
  const { authorId } = useParams<{ authorId: string }>();
  const posts = usePosts('',authorId);

  const history = useHistory();

  const handleItemClick = (slug?: string) => {
    history.push(`/post/${slug}`);
  };

  return (
    <div className="row row-cols-1 row-cols-md-2 g-4">
      {map(posts, (post) => (
        <div className="col post-card" key={post._id}>
          <PostItem item={post} onClick={handleItemClick} />
        </div>
      ))}
    </div>
  )
};
