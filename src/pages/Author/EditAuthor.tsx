import {useParams} from "react-router-dom";
import {useAuthor} from "./Author";
import {useEffect, useState} from "react";
import {UserModel} from "../../../server/models/UserModel";
import {addAuthor, editAuthor} from "../../api/authors";

const initialAuthor = {_id:'',firstName: '',lastName: '',email:'',bio: '',phone: '',isVerified:false,birthday: ''}

export const EditAuthorPage=()=>{

    const { authorId } = useParams<{ authorId: string }>();
    const [author, setAuthor] = useState({} as Partial<UserModel>);
    const _author= useAuthor(authorId)
    let mode

    useEffect(() => {
        if(authorId)
        {
            setAuthor(_author)
        }

    }, [_author]);

    const handleChange=(e:React.FormEvent<HTMLInputElement>)=>{
        const _name=e.currentTarget.name;
        const _value=e.currentTarget.value;
        setAuthor({...author,[_name]:_value});
    }
    const handleSubmit=()=>{
        if(authorId)
        {
            editAuthor(author);
        }
        else
        {
            addAuthor(author);
        }
    }

    return(
        <form>
            <div className="field">
                <label htmlFor="firstName">FirstName</label>
                <input
                    type="text"
                    id="firstName"
                    name="firstName"
                    value={author.firstName}
                    onChange={handleChange}
                />
            </div>
            <div className="field">
                <label htmlFor="email">lastName</label>
                <input
                    type="text"
                    id="lastName"
                    name="lastName"
                    value={author.lastName}
                    onChange={handleChange}
                />
            </div>
            <div className="field">
                <label htmlFor="score">email</label>
                <input
                    type="email"
                    id="email"
                    name="email"
                    value={author.email}
                    onChange={handleChange}
                />
            </div>
            <button type="submit" onClick={handleSubmit}>Save</button>
        </form>
    )
}