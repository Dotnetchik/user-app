import {useHistory, useParams} from "react-router-dom";
import {first} from "lodash";
import {usePosts} from "../Posts/PostsPage";
import {useAuthors} from "../Authors/AuthorsPage";
import React, {useEffect, useState} from "react";
import {UserModel} from "../../../server/models/UserModel";
import {getAuthor} from "../../api/authors";
import format from "date-fns/format";

export function useAuthor(id:string=''){
    const [author, setAuthor] = useState({} as Partial<UserModel>);

    useEffect(() => {
        getAuthor({id}).then(setAuthor);
    }, [id]);
    return author;
}

export const  AuthorPage=()=>{

    const { id } = useParams<{ id: string }>();
    const author= useAuthor(id)
    const history = useHistory();

    const handlePostClick = (id?: string) => {
        history.push(`/author/${id}/posts`);
    };

    if(!author)
    {
        return (
            <div>
                Author not found
            </div>
        )
    }
    return(
        <div>
            <div> Name:{author._id}</div>
            <br/>
            <div> Name:{author.firstName}</div>
            <br/>
            <div> LastName:{author.lastName}</div>
            <br/>
            <div> Birthday:{author.birthday}</div>
            <br/>
            <div> Email:{author.email}</div>
            <br/>
            <div> Phone:{author.phone}</div>
            <br/>
            <div> Bio:{author.bio}</div>
            <button onClick={()=>{handlePostClick(String(author._id))}}>View all posts</button>
        </div>

    )
}