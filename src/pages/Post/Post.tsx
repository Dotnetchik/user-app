import React from 'react';
import {useHistory, useParams} from 'react-router-dom';
import {usePosts} from "../Posts/PostsPage";
import {first,map} from "lodash";
export const PostPage = () => {
  const { slug } = useParams<{ slug: string }>();
  const post=first(usePosts(slug))
  const history=useHistory();
  const handleAuthorClick=(id:string)=>{
    history.push(`/author/${id}`);
  }
  if(!post)
  {
    return (
        <div>
          Post not found
        </div>
    )
  }
  return (
      <div>
        Title: {post?.title}
        Title: {map(post?.author, (author) => {
          return(
              <div key={String(author._id)} onClick={()=>{handleAuthorClick(String(author._id))}}>
                Name: {author.firstName}
                <br/>
                LastName: {author.lastName}
              </div>
          )
          }
      )}
      </div>
  )
}
