import {usePosts} from "../Posts/PostsPage";
import {useParams} from "react-router-dom";


export const AuthorPostPage=()=>{
    const { authorId } = useParams<{ authorId: string }>();
    const posts = usePosts("",authorId);
}