import React, {useEffect, useState} from "react";
import {UserModel} from "../../../server/models/UserModel";
import {getAuthors} from "../../api/authors";
import map from 'lodash/map';
import { useHistory } from 'react-router-dom';


export function useAuthors(){
    const [authors, setAuthors] = useState([] as Array<Partial<UserModel>>);

    useEffect(() => {
        getAuthors().then(setAuthors);
    }, []);
    return authors;
}

export const AuthorsPage=()=>{
    const authors=useAuthors();
    const history = useHistory();

    const handleAuthorClick = (id?: string) => {
        history.push(`/author/${id}`);
    };
    const handleEditAuthorClick = (id?: string) => {

        history.push(`/author/edit/${id}`);
    };
    const handleAddAuthorClick = () => {

        history.push(`/author/edit/`);
    };
    return (
        <div>
            <button onClick={handleAddAuthorClick}>Add</button>
            <table className="table">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>LastName</th>
                    <th>Email</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                {map(authors, (author) => (
                    <tr key={String(author._id)} onClick={()=>{
                        handleAuthorClick(String(author._id));
                    }}>
                        <td>{author.firstName}</td>
                        <td>{author.lastName}</td>
                        <td>{author.email}</td>
                        <td>
                            <button onClick={(event)=>{
                                event.stopPropagation();
                                handleEditAuthorClick(String(author._id));
                            }}>Edit</button>
                        </td>

                    </tr>

                ))}
                </tbody>
            </table>
        </div>

    )

}