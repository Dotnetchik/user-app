import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import { Menu } from './components/Menu/Menu';
import { PostsPage } from './pages/Posts/PostsPage';
import { PostPage } from './pages/Post/Post';
import './App.css';
import {AuthorsPage} from "./pages/Authors/AuthorsPage";
import {AuthorPage} from "./pages/Author/Author";
import {EditAuthorPage} from "./pages/Author/EditAuthor";


function App() {
  return (
    <div className="container app">
      <Router>
        <Menu />
        <main>
          <Switch>
            <Route exact path="/posts" component={PostsPage} />
            <Route exact path="/author/:authorId/posts" component={PostsPage} />
            <Route exact path="/author/edit/" component={EditAuthorPage} />
            <Route exact path="/post/:slug" component={PostPage} />
            <Route exact path="/author/:id" component={AuthorPage} />
            <Route exact path="/author/edit/:authorId" component={EditAuthorPage} />

            <Route exact path="/authors" component={AuthorsPage}>
            </Route>
            <Route path="/">
              <h1>Welcome to User App</h1>
            </Route>
          </Switch>
        </main>
      </Router>
    </div>
  );
}

export default App;
