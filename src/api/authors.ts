import { UserModel } from '../../server/models/UserModel';
import axios from 'axios';

const client = axios.create({
    baseURL: 'http://localhost:3001/'
});
interface IAuthorFilter{
    id:string
}

export const getAuthors=async (): Promise<Array<UserModel>> =>{

    const { data } = await client.get("/user");
    return data;
}
export const getAuthor=async (filter:Partial<IAuthorFilter>={}): Promise<UserModel> =>{
    const id=filter.id;
    const { data } = await client.get(`/user/${id}`);
    return data;
}
export const addAuthor=async (user:Partial<UserModel>={}): Promise<UserModel> =>{

    const { data } = await client.post(`/user`,user);
    return data;
}
export const editAuthor=async (user:Partial<UserModel>={}): Promise<UserModel> =>{

    const { data } = await client.put(`/user/${user._id}`,user);
    return data;
}