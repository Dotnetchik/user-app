import { PostModel } from '../../server/models/PostModel';
import axios from 'axios';

const client = axios.create({
  baseURL: 'http://localhost:3001/'
});
interface IPostsFilter {
  slug:string;
  authorId:string;
}

export const getPosts = async (filter:Partial<IPostsFilter>={}): Promise<Array<PostModel>> => {
  const{slug,authorId}=filter;
  const { data } = await client.get('/post',{
    params:filter
  });
  return data;
};
